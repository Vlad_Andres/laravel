<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
        $title = "Titling";
        $data = array(
            'title'=>'Titling',
            'id'=> ['Web', 'Prog','CEO']
        );
        return view('pages.index')->with($data);
    }
    public function contacts(){
        return view('pages.contacts');
    }
}
